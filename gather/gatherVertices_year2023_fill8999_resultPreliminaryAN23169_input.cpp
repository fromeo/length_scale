#include <utility>

//Constant lenght scale
//X
const int runnumber_clsX = 369802;
const int nSteps_clsX = 10;
const std::pair<int, int> timestamps_clsX[nSteps_clsX] = { //timestamps seems to be in GVA time, which is +1h from UTC (the reference time used in CMS OMS)
{1688056988, 1688057032},
{1688057077, 1688057125},
{1688057168, 1688057217},
{1688057262, 1688057310},
{1688057354, 1688057402},
{1688057461, 1688057508},
{1688057553, 1688057601},
{1688057645, 1688057693},
{1688057738, 1688057785},
{1688057830, 1688057878}
};
const int lumisections_clsX[nSteps_clsX] = {
3234, 3238, 3242, 3246, 3250, 3255, 3259, 3263, 3267, 3271
//3235, 3239, 3243, 3247, 3251, 3256, 3260, 3264, 3268, 3272
};
//Y
const int runnumber_clsY = 369802;
const int nSteps_clsY = 10;
const std::pair<int, int> timestamps_clsY[nSteps_clsY] = {
{1688058611, 1688058656},
{1688058701, 1688058748},
{1688058793, 1688058841},
{1688058886, 1688058933},
{1688058981, 1688059028},
{1688059087, 1688059134},
{1688059181, 1688059227},
{1688059272, 1688059320},
{1688059364, 1688059412},
{1688059457, 1688059504}

//{1688058625, 1688058654},
//{1688058713, 1688058756},
//{1688058815, 1688058844},
//{1688058902, 1688058931},
//{1688059004, 1688059037},
//{1688059120, 1688059135},
//{1688059208, 1688059222},
//{1688059295, 1688059324},
//{1688059383, 1688059412},
////{1688059499, 1688059514} // '2023-06-29 19:24:59' '2023-06-29 19:25:14' //-2081.03
//{1688059485, 1688059499} //'2023-06-29 19:24:45' '2023-06-29 19:24:59' //-2083.48
//// {1688059470, 1688059500}  //'2023-06-29 19:24:45' +- 15sec             //-2083.05
//// {1688059470, 1688059485} //'2023-06-29 19:24:30' '2023-06-29 19:24:45' //-2082.52
//// {1688059470, 1688059499} // '2023-06-29 19:24:30''2023-06-29 19:24:59' //-2083
//// {1688059470, 1688059514} // '2023-06-29 19:24:30''2023-06-29 19:25:14' //-2082.43
};
const int lumisections_clsY[nSteps_clsY] = {
3304, 3308, 3312, 3316, 3319, 3324, 3328, 3332, 3336, 3340
//3305, 3309, 3313, 3317, 3321, 3326, 3329, 3334, 3337, 3341
};

//Variable lenght scale
//X1
const int runnumber_vlsX1A = 369802;
const int nSteps_vlsX1A = 5;
const std::pair<int, int> timestamps_vlsX1A[nSteps_vlsX1A] = { 
{1688059900, 1688059943},
{1688060117, 1688060158},
{1688060333, 1688060374},
{1688060549, 1688060589},
{1688060764, 1688060805}
};
const int lumisections_vlsX1A[nSteps_vlsX1A] = {
3359, 3368, 3378, 3387, 3396   
};
const int runnumber_vlsX1B = 369802;
const int nSteps_vlsX1B = 5;
const std::pair<int, int> timestamps_vlsX1B[nSteps_vlsX1B] = { 
{1688060044, 1688060087},
{1688060260, 1688060302},
{1688060476, 1688060518},
{1688060691, 1688060734}, 
{1688060907, 1688060949}
};
const int lumisections_vlsX1B[nSteps_vlsX1B] = {
3366, 3375, 3384, 3393, 3402 
};
//Y1
const int runnumber_vlsY1A = 369802;
const int nSteps_vlsY1A = 5;
const std::pair<int, int> timestamps_vlsY1A[nSteps_vlsY1A] = { 
{1688061367, 1688061410}, 
{1688061579, 1688061621},
{1688061790, 1688061831},
{1688062001, 1688062043},
{1688062212, 1688062255}
};
const int lumisections_vlsY1A[nSteps_vlsY1A] = {
3422, 3431, 3440, 3449, 3458              
};
const int runnumber_vlsY1B = 369802;
const int nSteps_vlsY1B = 5;
const std::pair<int, int> timestamps_vlsY1B[nSteps_vlsY1B] = { 
{1688061506, 1688061548},
{1688061717, 1688061759}, 
{1688061928, 1688061971},
{1688062140, 1688062182},
{1688062352, 1688062393}
};
const int lumisections_vlsY1B[nSteps_vlsY1B] = {
3428, 3437, 3446, 3455, 3463 
};
//X2
const int runnumber_vlsX2A = 369802;
const int nSteps_vlsX2A = 5;
const std::pair<int, int> timestamps_vlsX2A[nSteps_vlsX2A] = { 
{1688062896, 1688062938}, 
{1688063108, 1688063151},
{1688063320, 1688063362},
{1688063532, 1688063575},
{1688063745, 1688063787}
};
const int lumisections_vlsX2A[nSteps_vlsX2A] = {
3488, 3497, 3506, 3515, 3524 
};
const int runnumber_vlsX2B = 369802;
const int nSteps_vlsX2B = 5;
const std::pair<int, int> timestamps_vlsX2B[nSteps_vlsX2B] = { 
{1688063036, 1688063078},
{1688063248, 1688063291},
{1688063461, 1688063502},
{1688063672, 1688063715},
{1688063885, 1688063926}
};
const int lumisections_vlsX2B[nSteps_vlsX2B] = {
3494, 3503, 3512, 3521, 3530
};
//Y2
const int runnumber_vlsY2A = 369802;
const int nSteps_vlsY2A = 5;
const std::pair<int, int> timestamps_vlsY2A[nSteps_vlsY2A] = { 
{1688064398, 1688064440},
{1688064616, 1688064657},
{1688064834, 1688064874},
{1688065051, 1688065093},
{1688065269, 1688065310}
};
const int lumisections_vlsY2A[nSteps_vlsY2A] = {
3552, 3561, 3571, 3580, 3589
};
const int runnumber_vlsY2B = 369802;
const int nSteps_vlsY2B = 5;
const std::pair<int, int> timestamps_vlsY2B[nSteps_vlsY2B] = { 
{1688064544, 1688064584},
{1688064761, 1688064803},
{1688064978, 1688065020},
{1688065196, 1688065237},
{1688065415, 1688065456}
};
const int lumisections_vlsY2B[nSteps_vlsY2B] = {
3558, 3568, 3577, 3587, 3596     
};
