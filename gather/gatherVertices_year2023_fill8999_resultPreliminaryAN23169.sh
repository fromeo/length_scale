#mkdir -p Fill8999/gather/
#root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(0)'
#python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_clsX.root 10 5 > Fill8999/gather/fill8999_clsX.txt

#root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(1)'
#python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_clsY.root 10 5 > Fill8999/gather/fill8999_clsY.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(2)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsX1A.root 5 5 > Fill8999/gather/fill8999_vlsX1A.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(3)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsX1B.root 5 5 > Fill8999/gather/fill8999_vlsX1B.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(4)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsY1A.root 5 5 > Fill8999/gather/fill8999_vlsY1A.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(5)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsY1B.root 5 5 > Fill8999/gather/fill8999_vlsY1B.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(6)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsX2A.root 5 5 > Fill8999/gather/fill8999_vlsX2A.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(7)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsX2B.root 5 5 > Fill8999/gather/fill8999_vlsX2B.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(8)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsY2A.root 5 5 > Fill8999/gather/fill8999_vlsY2A.txt

root -l -q -b 'gather/gatherVertices_year2023_fill8999_resultPreliminaryAN23169.cpp+(9)'
python3 gather/gatherVerticesSummary.py Fill8999/gather/fill8999_vlsY2B.root 5 5 > Fill8999/gather/fill8999_vlsY2B.txt
