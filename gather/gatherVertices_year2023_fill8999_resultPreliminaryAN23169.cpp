#include <string>

#include "gatherVertices.cpp"
#include "gatherVertices_year2023_fill8999_resultPreliminaryAN23169_input.cpp"

const std::string inputfiles = "Fill8999/prepare/fill8999_ZeroBias*.root";

const std::string xobs = "x";
const std::string yobs = "y";
const std::string cond = "nTrk>=14";

const std::string outputprefix = "Fill8999/gather/fill8999_";

void gatherVertices_year2023_fill8999_resultPreliminaryAN23169(int iScan=-1) {
    if(iScan>9) {
        std::cout << "Specify a number between 0 and 9" << std::endl;
        return;
    }
    if(iScan<0 || iScan==0) gatherVerticesScan(inputfiles, outputprefix+"clsX.root", "clsX", xobs, nSteps_clsX, timestamps_clsX, runnumber_clsX, lumisections_clsX, cond);
    if(iScan<0 || iScan==1) gatherVerticesScan(inputfiles, outputprefix+"clsY.root", "clsY", yobs, nSteps_clsY, timestamps_clsY, runnumber_clsY, lumisections_clsY, cond);
    if(iScan<0 || iScan==2) gatherVerticesScan(inputfiles, outputprefix+"vlsX1A.root", "vlsX1A", xobs, nSteps_vlsX1A, timestamps_vlsX1A, runnumber_vlsX1A, lumisections_vlsX1A, cond);
    if(iScan<0 || iScan==3) gatherVerticesScan(inputfiles, outputprefix+"vlsX1B.root", "vlsX1B", xobs, nSteps_vlsX1B, timestamps_vlsX1B, runnumber_vlsX1B, lumisections_vlsX1B, cond);   
    if(iScan<0 || iScan==4) gatherVerticesScan(inputfiles, outputprefix+"vlsY1A.root", "vlsY1A", yobs, nSteps_vlsY1A, timestamps_vlsY1A, runnumber_vlsY1A, lumisections_vlsY1A, cond);
    if(iScan<0 || iScan==5) gatherVerticesScan(inputfiles, outputprefix+"vlsY1B.root", "vlsY1B", yobs, nSteps_vlsY1B, timestamps_vlsY1B, runnumber_vlsY1B, lumisections_vlsY1B, cond);   
    if(iScan<0 || iScan==6) gatherVerticesScan(inputfiles, outputprefix+"vlsX2A.root", "vlsX2A", xobs, nSteps_vlsX2A, timestamps_vlsX2A, runnumber_vlsX2A, lumisections_vlsX2A, cond);
    if(iScan<0 || iScan==7) gatherVerticesScan(inputfiles, outputprefix+"vlsX2B.root", "vlsX2B", xobs, nSteps_vlsX2B, timestamps_vlsX2B, runnumber_vlsX2B, lumisections_vlsX2B, cond);   
    if(iScan<0 || iScan==8) gatherVerticesScan(inputfiles, outputprefix+"vlsY2A.root", "vlsY2A", yobs, nSteps_vlsY2A, timestamps_vlsY2A, runnumber_vlsY2A, lumisections_vlsY2A, cond);
    if(iScan<0 || iScan==9) gatherVerticesScan(inputfiles, outputprefix+"vlsY2B.root", "vlsY2B", yobs, nSteps_vlsY2B, timestamps_vlsY2B, runnumber_vlsY2B, lumisections_vlsY2B, cond);
}
