# Load required modules
import numpy as np
import matplotlib.pyplot as plt
import h5py
import tables as t

# indicate the hd5 file where the scan of interest is
## Constant LS
beam1first = True 
#2022 
#title_plot = 'pp 2022 CLS in x'
#filename = "/brildata/vdmdata22/8381/8381_221111121259_221111122929.hd5" #note the whole time in the file name is in UTC, while the timestamp is in local time (UTC+1h)
#title_plot = 'pp 2022 CLS in y'
#filename = "/brildata/vdmdata22/8381/8381_221111123525_221111125329.hd5" 
#2023 
#title_plot = 'pp 2023 CLS in x'
#filename = "/brildata/vdmdata23/8999/8999_230629164148_230629165918.hd5"
#title_plot = 'pp 2023 CLS in y'
#filename = "/brildata/vdmdata23/8999/8999_230629170846_230629172620.hd5"

## Variable LS
#2023
beam1first = False
#title_plot = 'pp 2023 VLS in X1' 
#filename = "/brildata/vdmdata23/8999/8999_230629172951_230629175056.hd5"
#title_plot = 'pp 2023 VLS in Y1'
#filename = "/brildata/vdmdata23/8999/8999_230629175427_230629181452.hd5"
beam1first = True
#title_plot = 'pp 2023 VLS in X2'
#filename = "/brildata/vdmdata23/8999/8999_230629181952_230629184026.hd5"
title_plot = 'pp 2023 VLS in Y2'
filename = "/brildata/vdmdata23/8999/8999_230629184449_230629190603.hd5"

## Coodinates
#X
#beam1 = 's_xingP1'
#beam2 = 's_xingP2' 
#Y
beam1 = 's_sepP1'
beam2 = 's_sepP2' 

# get it and the vdmscan table where the info about the beam position is 
h5file = h5py.File(filename, "r")
vdmscan = h5file["vdmscan"]

# find ranges of scan steps 
if(beam1first): values_indices = np.where(np.diff(np.array(vdmscan[beam1])) != 0)[0]
else: values_indices = np.where(np.diff(np.array(vdmscan[beam2])) != 0)[0]
values = np.insert(values_indices, 0, 0)
#print(values)
pairs = []
for i in range(len(values)-1):
    if(i==0): pairs.append((values[i], values[i+1]))
    else: pairs.append((values[i]+3, values[i+1]))


# plot values to make sure your are doing correctly
fig, ax = plt.subplots(figsize=(15,5))

ax.tick_params(top=True, right=True, direction='in', labelsize=14)
ax.set_xlabel('Time [s]', size=20)
ax.set_ylabel('Position [mm]', size=20)

timestamps = vdmscan['timestampsec'] - np.min(vdmscan['timestampsec'])

if(beam1first):
 ax.scatter(timestamps, vdmscan[beam1], marker='+', label='Beam 1', c='blue')
 ax.scatter(timestamps, vdmscan[beam2], marker='+', label='Beam 2', c='red')
else:
 ax.scatter(timestamps, vdmscan[beam2], marker='+', label='Beam 2', c='red')
 ax.scatter(timestamps, vdmscan[beam1], marker='+', label='Beam 1', c='blue')

ax.legend(title=title_plot, fontsize=10, loc='upper right')#, ncol=2)

# Plot vertical lines for each pair of indices
pairs_timestamp = []
pairs_lsnum = []
pairs_beam1 = []
pairs_beam2 = []
for pair in pairs:
    start_timestamp = vdmscan['timestampsec'][pair[0]] - np.min(vdmscan['timestampsec'])
    end_timestamp = vdmscan['timestampsec'][pair[1]] - np.min(vdmscan['timestampsec'])
    ax.axvline(x=start_timestamp, color='black', linestyle='--', label='Start of same values')
    ax.axvline(x=end_timestamp, color='black', linestyle='--', label='End of same values')
    pairs_timestamp.append((vdmscan['timestampsec'][pair[0]],vdmscan['timestampsec'][pair[1]]))
    pairs_lsnum.append((vdmscan['lsnum'][pair[0]],vdmscan['lsnum'][pair[1]]))
    pairs_beam1.append(vdmscan[beam1][pair[0]]*1000) #times 1000 to convert mm in um
    pairs_beam2.append(vdmscan[beam2][pair[0]]*1000)

print(title_plot)
print("")
#lumi sections
print('lumi sections')
print(pairs_lsnum)
print('lumi sections A')
print(pairs_lsnum[1], pairs_lsnum[4], pairs_lsnum[7], pairs_lsnum[10], pairs_lsnum[13])
print('lumi sections B')
print(pairs_lsnum[3], pairs_lsnum[6], pairs_lsnum[9], pairs_lsnum[12], pairs_lsnum[15])
print("")
#timestamp
print('timestamp')
print(pairs_timestamp)
print('timestamp A')
print(pairs_timestamp[1], pairs_timestamp[4], pairs_timestamp[7], pairs_timestamp[10], pairs_timestamp[13])
print('timestamp B')
print(pairs_timestamp[3], pairs_timestamp[6], pairs_timestamp[9], pairs_timestamp[12], pairs_timestamp[15])
print("")
#beams
print('position beam 1')
print(pairs_beam1)
print('position beam 2')
print(pairs_beam2)
print('position beam 1 A')
print(pairs_beam1[1], pairs_beam1[4], pairs_beam1[7], pairs_beam1[10], pairs_beam1[13])
print('position beam 2 A')
print(pairs_beam2[1], pairs_beam2[4], pairs_beam2[7], pairs_beam2[10], pairs_beam2[13])
print('position beam 1 B')
print(pairs_beam1[3], pairs_beam1[6], pairs_beam1[9], pairs_beam1[12], pairs_beam1[15])
print('position beam 2 B')
print(pairs_beam2[3], pairs_beam2[6], pairs_beam2[9], pairs_beam2[12], pairs_beam2[15])

# Display the plot
plt.show()
