import positions

nSteps = {'clsX': 10, 'clsY': 10, 'vlsX1A': 5, 'vlsX1B': 5, 'vlsY1A': 5, 'vlsY1B': 5, 'vlsX2A': 5, 'vlsX2B': 5, 'vlsY2A': 5, 'vlsY2B': 5}
split = {'clsX': 5, 'clsY': 5, 'vlsX1A': 5, 'vlsX1B': 5, 'vlsY1A': 5, 'vlsY1B': 5, 'vlsX2A': 5, 'vlsX2B': 5, 'vlsY2A': 5, 'vlsY2B': 5}
horizontal = {'clsX': True, 'clsY': False, 'vlsX1A': True, 'vlsX1B': True, 'vlsY1A': False, 'vlsY1B': False, 'vlsX2A': True, 'vlsX2B': True, 'vlsY2A': False, 'vlsY2B': False}
nominal = {
    1: {
        'clsX': [-240.73241651058197, -144.4394439458847, -48.146482557058334, 48.146482557058334, 144.4394439458847, 279.24960851669312, 182.95663595199585, 86.663670837879181, -9.629296138882637, -105.92225939035416],
        'clsY': [-240.73241651058197, -144.4394439458847, -48.146482557058334, 48.146482557058334, 144.4394439458847, 279.24960851669312, 182.95663595199585, 86.663670837879181, -9.629296138882637, -105.92225939035416],
        'vlsX1A': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197],
        'vlsX1B': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197],
        'vlsY1A': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197],
        'vlsY1B': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197], 
        'vlsX2A': [-361.09861731529236, -240.73241651058197, -120.36620825529099, 2.7755575615628914e-14, 120.36620825529099],
        'vlsX2B': [-120.36620825529099, 2.7755575615628914e-14, 120.36620825529099, 240.73241651058197, 361.09861731529236],
        'vlsY2A': [-361.09861731529236, -240.73241651058197, -120.36620825529099, 2.7755575615628914e-14, 120.36620825529099],
        'vlsY2B': [-120.36620825529099, 2.7755575615628914e-14, 120.36620825529099, 240.73241651058197, 361.09861731529236] 
    },
    2: {
        'clsX': [-105.92225939035416, -9.629296138882637, 86.663670837879181, 182.95663595199585, 279.24960851669312, 144.4394439458847, 48.146482557058334, -48.146482557058334, -144.4394439458847, -240.73241651058197],
        'clsY': [-105.92225939035416, -9.629296138882637, 86.663670837879181, 182.95663595199585, 279.24960851669312, 144.4394439458847, 48.146482557058334, -48.146482557058334, -144.4394439458847, -240.73241651058197],
        'vlsX1A': [-361.09861731529236, -240.73241651058197, -120.36620825529099, 2.7755575615628914e-14, 120.36620825529099],
        'vlsX1B': [-120.36620825529099, 2.7755575615628914e-14, 120.36620825529099, 240.73241651058197, 361.09861731529236],
        'vlsY1A': [-361.09861731529236, -240.73241651058197, -120.36620825529099, 2.7755575615628914e-14, 120.36620825529099],
        'vlsY1B': [-120.36620825529099, 2.7755575615628914e-14, 120.36620825529099, 240.73241651058197, 361.09861731529236], 
        'vlsX2A': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197],
        'vlsX2B': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197],
        'vlsY2A': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197],
        'vlsY2B': [-240.73241651058197, -120.36620825529099, 0.0, 120.36620825529099, 240.73241651058197]    
    },
}
timestamps = {
    'clsX': [
[1688056988, 1688057032],
[1688057077, 1688057125],
[1688057168, 1688057217],
[1688057262, 1688057310],
[1688057354, 1688057402],
[1688057461, 1688057508],
[1688057553, 1688057601],
[1688057645, 1688057693],
[1688057738, 1688057785],
[1688057830, 1688057878]
    ],
    'clsY': [
[1688058611, 1688058656],
[1688058701, 1688058748],
[1688058793, 1688058841],
[1688058886, 1688058933],
[1688058959, 1688059028],
[1688059087, 1688059134],
[1688059181, 1688059227],
[1688059272, 1688059320],
[1688059364, 1688059412],
[1688059457, 1688059504]
    ],
    'vlsX1A': [
[1688059900, 1688059943],
[1688060117, 1688060158],
[1688060333, 1688060374],
[1688060549, 1688060589],
[1688060764, 1688060805]
        ],
    'vlsX1B': [
[1688060044, 1688060087],
[1688060260, 1688060302],
[1688060476, 1688060518],
[1688060691, 1688060734], 
[1688060907, 1688060949]
        ],
    'vlsY1A': [
[1688061367, 1688061410], 
[1688061579, 1688061621],
[1688061790, 1688061831],
[1688062001, 1688062043],
[1688062212, 1688062255]
        ],
    'vlsY1B': [
[1688061506, 1688061548],
[1688061717, 1688061759], 
[1688061928, 1688061971],
[1688062140, 1688062182],
[1688062352, 1688062393]
        ], 
    'vlsX2A': [
[1688062896, 1688062938], 
[1688063108, 1688063151],
[1688063320, 1688063362],
[1688063532, 1688063575],
[1688063745, 1688063787]
            ],
    'vlsX2B': [
[1688063036, 1688063078],
[1688063248, 1688063291],
[1688063461, 1688063502],
[1688063672, 1688063715],
[1688063885, 1688063926]
            ],
    'vlsY2A': [
[1688064398, 1688064440],
[1688064616, 1688064657],
[1688064834, 1688064874],
[1688065051, 1688065093],
[1688065269, 1688065310]
            ],
    'vlsY2B': [
[1688064544, 1688064584],
[1688064761, 1688064803],
[1688064978, 1688065020],
[1688065196, 1688065237],
[1688065415, 1688065456]
            ] 
}

def positions8999(scan):
    # nominal positions
    if horizontal[scan]:
        beams_nominal = zip(nominal[1][scan], [0.0]*nSteps[scan], nominal[2][scan], [0.0]*nSteps[scan])
    else:
        beams_nominal = zip([0.0]*nSteps[scan], nominal[1][scan], [0.0]*nSteps[scan], nominal[2][scan])
    bs_nominal = positions.averagePositions(zip(nominal[1][scan], nominal[2][scan]))

    ## DOROS positions
    #bs_doros, bs_doros_unc, bs_nominal_corr_doros_old = positions.readAndApplyDoros(
    #    'Fill6381/positions/Fill6381Doros.txt',
    #    bs_nominal, timestamps[scan], horizontal[scan], split[scan]
    #)
    #bs_nominal_corr_doros5_central, bs_nominal_corr_doros5_up, \
    #bs_nominal_corr_doros5_down, log_doros5 = positions.applyCorrectionFromAverageWithoutOutliers(
    #    bs_doros, bs_doros_unc, bs_nominal, split[scan], initial=5
    #)
    #bs_nominal_corr_doros6_central, bs_nominal_corr_doros6_up, \
    #bs_nominal_corr_doros6_down, log_doros6 = positions.applyCorrectionFromAverageWithoutOutliers(
    #    bs_doros, bs_doros_unc, bs_nominal, split[scan], initial=6
    #)
    ## arc positions
    #bs_nominal_arc, bs_nominal_arc_unc, bs_nominal_corr_arc_old = positions.readAndApplyArc(
    #    'Fill6381/positions/Fill6381Arc.txt',
    #    beams_nominal, timestamps[scan], horizontal[scan], split[scan]
    #)
    #bs_nominal_corr_arc5_central, bs_nominal_corr_arc5_up, \
    #bs_nominal_corr_arc5_down, log_arc5 = positions.applyCorrectionFromAverageWithoutOutliers(
    #    bs_nominal_arc, bs_nominal_arc_unc, bs_nominal, split[scan], initial=5
    #)
    #bs_nominal_corr_arc6_central, bs_nominal_corr_arc6_up, \
    #bs_nominal_corr_arc6_down, log_arc6 = positions.applyCorrectionFromAverageWithoutOutliers(
    #    bs_nominal_arc, bs_nominal_arc_unc, bs_nominal, split[scan], initial=6
    #)
    
    # vertex positions
    bs_vtx, bs_nominal_corr_vtx = positions.readAndApplyVertex(
        'Fill8999/gather/fill8999_{}.txt'.format(scan),
        bs_nominal, horizontal[scan], split[scan]
    )
    # write results
    #with open('Fill6381/positions/fill8999_Scan{}_OutlierRemoval.txt'.format(scan), 'w') as f:
    #    f.write(log_doros5.replace('#', '#DOROS5'))
    #    f.write('\n\n\n')
    #    f.write(log_doros6.replace('#', '#DOROS6'))
    #    f.write('\n\n\n')
    #    f.write(log_arc5.replace('#', '#ARC5'))
    #    f.write('\n\n\n')
    #    f.write(log_arc6.replace('#', '#ARC6'))
    with open('Fill8999/positions/fill8999_{}.txt'.format(scan), 'w') as f:
        f.write('#nominal nominal_corr_vtx ')
        #f.write('doros nominal_corr_doros_old ')
        #f.write('nominal_corr_doros5 nominal_corr_doros5_up nominal_corr_doros5_down ')
        #f.write('nominal_corr_doros6 nominal_corr_doros6_up nominal_corr_doros6_down ')
        #f.write('nominal_arc nominal_corr_arc_old ')
        #f.write('nominal_corr_arc5 nominal_corr_arc5_up nominal_corr_arc5_down ')
        #f.write('nominal_corr_arc6 nominal_corr_arc6_up nominal_corr_arc6_down ')
        f.write('\n')
        values = list(zip(
            bs_nominal, bs_nominal_corr_vtx,
            #bs_doros, bs_nominal_corr_doros_old,
            #bs_nominal_corr_doros5_central, bs_nominal_corr_doros5_up, bs_nominal_corr_doros5_down,
            #bs_nominal_corr_doros6_central, bs_nominal_corr_doros6_up, bs_nominal_corr_doros6_down,
            #bs_nominal_arc, bs_nominal_corr_arc_old,
            #bs_nominal_corr_arc5_central, bs_nominal_corr_arc5_up, bs_nominal_corr_arc5_down,
            #bs_nominal_corr_arc6_central, bs_nominal_corr_arc6_up, bs_nominal_corr_arc6_down,
        ))
        for part in (values[:split[scan]], values[split[scan]:]):
            for vals in part:
                f.write(' '.join(('{}',)*len(vals)).format(*vals)+'\n')
            f.write('\n\n')

if __name__=='__main__':
    import sys
    if len(sys.argv)<2 or sys.argv[1] not in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'):
        print('Specify a number below 10')
        quit()
    if sys.argv[1]=='0':
        positions8999('clsX')
    elif sys.argv[1]=='1':
        positions8999('clsY')
    elif sys.argv[1]=='2':
        positions8999('vlsX1A')
    elif sys.argv[1]=='3':
        positions8999('vlsX1B')
    elif sys.argv[1]=='4':
        positions8999('vlsY1A')
    elif sys.argv[1]=='5':
        positions8999('vlsY1B')
    elif sys.argv[1]=='6':
        positions8999('vlsX2A')
    elif sys.argv[1]=='7':
        positions8999('vlsX2B')
    elif sys.argv[1]=='8':
        positions8999('vlsY2A')
    elif sys.argv[1]=='9':
        positions8999('vlsY2B')





