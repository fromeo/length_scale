import csv, math

def loadCsvBpm(filename):
    """ input:  filename of DOROS or ARC CSV file
        output: positions = [[B1h, B1v, B2h, B2v], ...]
                offsets = [[timestamp, B1h, B1v, B2h, B2v], ...]
    """
    positions, offsets = [], []
    with open(filename) as f:
        for row in f:
            if row.startswith('#'):
                offset = eval('{{{}}}'.format(row[1:-1]))
                offsets.append([offset.keys()[0]]+[v for v in offset.values()[0]])
            else:
                position = eval('[{}]'.format(row.replace(' ', ',')))
                positions.append(position)
    return positions, offsets

def loadCsvVtx(filename):
    """ input:  filename of vertex CSV file
        output: vtx = [[ts_avg, ...] [ts_fit, ...] [ls_avg, ...] [ls_fit, ...]]
                vtxerr = [[ts_avgerr, ...] [ts_fiterr, ...] [ls_avgerr, ...] [ls_fiterr, ...]]
    """
    vtx = [[], [], [], []]
    vtxerr = [[], [], [], []]
    with open(filename, 'r') as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            if not row or row[0].startswith('#'):
                continue
            for entry, pos in zip(row[::2], vtx):
                pos.append(float(entry))
            for entry, pos in zip(row[1::2], vtxerr):
                pos.append(float(entry))
    return vtx, vtxerr

def correctDoros(positions, offsets, signs=None):
    """ input:  positions = [[timestamp, B1hL, B1hR, B1vL, B1vR, B2hL, B2hR, B2vL, B2vR], ...]
                offsets = [[timestamp, B1h, B1v, B2h, B2v], ...]
        output: doros = [[timestamp, B1h, B1v, B2h, B2v], ...]
    """
    if signs is None:
        signs = (1,)*8
    offsettimes = (lambda t: zip(t, t[1:]+[None]))(sorted([o[0] for o in offsets]))
    doros = []
    for offsettime in offsettimes:
        offset = [o[1:] for o in offsets if o[0]==offsettime[0]][0]
        doros.extend([[
            p[0], # timestamp
            1000*0.5*(signs[0]*p[1]+signs[1]*p[2])-offset[0], # B1h
            1000*0.5*(signs[2]*p[3]+signs[3]*p[4])-offset[1], # B1v
            1000*0.5*(signs[4]*p[5]+signs[5]*p[6])-offset[2], # B2h
            1000*0.5*(signs[6]*p[7]+signs[7]*p[8])-offset[3], # B2v
        ] for p in positions if p[0]>=offsettime[0] if (
            offsettime[1] is None or p[0]<offsettime[1]
        )])
    return doros

def evaluateDoros(positions, offsets, timestamps, signs=None):
    """ input:  positions = [[timestamp, B1hL, B1hR, B1vL, B1vR, B2hL, B2hR, B2vL, B2vR], ...]
                offsets = [[timestamp, B1h, B1v, B2h, B2v], ...]
                timestamps = [[start, stop], ...]
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: doros = [[B1h, B1v, B2h, B2v], ...]
                doroserr = [[B1herr, B1verr, B2herr, B2verr], ...]
    """
    corrected = correctDoros(positions, offsets, signs)
    doros, doroserr = [], []
    for start, stop in timestamps:
        pos = [c[1:] for c in corrected if c[0]>=start if c[0]<=stop]
        if len(pos)==0:
            doros.append([None, None, None, None])
            doroserr.append([None, None, None, None])
            continue
        if len(pos)==1:
            doros.append([p for p in pos[0]])
            doroserr.append([None, None, None, None])
            continue
        mean = map(lambda p: sum(p)/len(p), zip(*pos))
        merr = [(
            sum([(p-m)**2 for p in pp])/len(pp)/(len(pp)-1)
        )**0.5 for pp, m in zip(zip(*pos), mean)]
        doros.append(mean)
        doroserr.append(merr)
    return doros, doroserr

def correctArc(positions, offsets, signs=None):
    """ input:  positions = [[timestamp, B1hL, B1hR, B1vL, B1vR, B2hL, B2hR, B2vL, B2vR], ...]
                offsets = [[timestamp, B1h, B1v, B2h, B2v], ...]
        output: arc = [[timestamp, B1h, B1v, B2h, B2v], ...]
    """
    if signs is None:
        signs = (1,)*8
    offsettimes = (lambda t: zip(t, t[1:]+[None]))(sorted([o[0] for o in offsets]))
    arc = []
    for offsettime in offsettimes:
        offset = [o[1:] for o in offsets if o[0]==offsettime[0]][0]
        arc.extend([[
            p[0], # timestamp
            0.5*(signs[0]*p[1]+signs[1]*p[2])-offset[0], # B1h
            0.5*(signs[2]*p[3]+signs[3]*p[4])-offset[1], # B1v
            0.5*(signs[4]*p[5]+signs[5]*p[6])-offset[2], # B2h
            0.5*(signs[6]*p[7]+signs[7]*p[8])-offset[3], # B2v
        ] for p in positions if p[0]>=offsettime[0] if (
            offsettime[1] is None or p[0]<offsettime[1]
        )])
    return arc

def evaluateArc(positions, offsets, timestamps, nominals, signs=None):
    """ input:  positions = [[timestamp, B1hL, B1hR, B1vL, B1vR, B2hL, B2hR, B2vL, B2vR], ...]
                offsets = [[timestamp, B1h, B1v, B2h, B2v], ...]
                timestamps = [[start, stop], ...]
                nominals = [[B1h, B1v, B2h, B2v], ...]
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: arc = [[B1h, B1v, B2h, B2v], ...]
                arcerr = [[B1herr, B1verr, B2herr, B2verr], ...]
    """
    corrected = correctArc(positions, offsets, signs)
    arc, arcerr = [], []
    for (start, stop), nominal in zip(timestamps, nominals):
        pos = [map(sum, zip(c[1:], nominal)) for c in corrected if c[0]>=start if c[0]<=stop]
        if len(pos)==0:
            arc.append([None, None, None, None])
            arcerr.append([None, None, None, None])
            continue
        if len(pos)==1:
            arc.append([p for p in pos[0]])
            arcerr.append([None, None, None, None])
            continue
        mean = map(lambda p: sum(p)/len(p), zip(*pos))
        merr = [(
            sum([(p-m)**2 for p in pp])/len(pp)/(len(pp)-1)
        )**0.5 for pp, m in zip(zip(*pos), mean)]
        arc.append(mean)
        arcerr.append(merr)
    return arc, arcerr

def splitDirections(positions, split=5):
    return positions[:split], positions[split:]

def averagePositions(positions):
    #for pos in positions: 
    #    print(pos)
    #    print(sum(pos))
    #    print(len(pos))
    return [sum(pos)/len(pos) for pos in positions]

def applyCorrectionFrom(positions, nominal_bs, split=5, reference=0):
    """ input:  positions = [BS, BS, ...]
                nominal_bs = [BS, BS, ...]
                split: number of steps of first scan direction
                reference=0: correct w.r.t. average step size (per direction)
                         =i: correct w.r.t. i-th step size (with i>0)
        output: nominal_corr = [BS, BS, ...]
    """
    stepsize = [[q-p for (p,q) in zip(pos, pos[1:])] for pos in splitDirections(positions, split)]
    stepsize = [arr for arr in stepsize if arr] #for vls we want to remove null arrays as []
    if reference==0:
        refstepsize = [sum(pos)/len(pos) for pos in stepsize]
    elif reference<=len(stepsize[0]):
        theref = stepsize[0][reference-1]
        refstepsize = [theref, -theref]
    else:
        theref = stepsize[1][reference-len(stepsize[0])-1]
        refstepsize = [-theref, theref]
    refposition = [int(math.ceil((len(pos)+1.0)/2)) for pos in stepsize]
    correction = [[p-ref for p in pos] for pos, ref in zip(stepsize, refstepsize)]
    cumulcorr = [[
        sum(corr[start:i])-sum(corr[i:start]) for i in range(len(corr)+1)
    ] for corr, start in zip(correction, refposition)]
    if(len(cumulcorr)==2): nominal_corr = [nom+corr for nom, corr in zip(nominal_bs, cumulcorr[0]+cumulcorr[1])]
    if(len(cumulcorr)==1): nominal_corr = [nom+corr for nom, corr in zip(nominal_bs, cumulcorr[0])]
    return nominal_corr

def applyCorrectionFromAverageWithoutOutliers(positions, positions_unc, nominal_bs, split=5, initial=5):
    """ input:  positions = [BS, BS, ...]
                positions_unc = [BS_unc, BS_unc, ...]
                nominal_bs = [BS, BS, ...]
                split: number of steps of first scan direction
        output: nominal_corr = [BS, BS, ...]
                nominal_corr_up = [BS, BS, ...]
                nominal_corr_down = [BS, BS, ...]
                log: information about the outlier removal
    """
    stepsize = [[q-p for (p,q) in zip(pos, pos[1:])] for pos in splitDirections(positions, split)]
    stepsizeunc = [[(p**2+q**2)**0.5 for (p,q) in zip(pos, pos[1:])] for pos in splitDirections(positions_unc, split)]
    diffs = sorted(map(abs, stepsize[0]+stepsize[1]))
    means = [sum(diffs[i:i+initial])/initial for i in range(9-initial)]
    stddevs = [(sum([(d-means[i])**2 for d in diffs[i:i+initial]])*1.0/(initial-1))**0.5 for i in range(9-initial)]
    i_best = min(range(9-initial), key=stddevs.__getitem__)
    mini, maxi = means[i_best]-3*stddevs[i_best], means[i_best]+3*stddevs[i_best]
    selected = [d for d in diffs if d>=mini and d<=maxi]
    meanstep = sum(selected)/len(selected)
    stddevsq = sum([(s-meanstep)**2 for s in selected])/(len(selected)-1)
    refstepsizes = [meanstep, meanstep-stddevsq**0.5, meanstep+stddevsq**0.5]
    log = '\n'.join([
        '# low outliers < {}'.format(mini),
        '# high outliers > {}'.format(maxi),
        '# removed outliers: {}'.format(len(diffs)-len(selected)),
        '# mean step size: {}'.format(meanstep),
        '# standard deviation of step sizes: {}'.format(stddevsq**0.5),
        '# smallest considered step size: {}'.format(refstepsizes[1]),
        '# largest considered step size: {}'.format(refstepsizes[2]),
    ])
    for diff, unc in zip(map(abs, stepsize[0]+stepsize[1]), stepsizeunc[0]+stepsizeunc[1]):
        log += '\n{} {} {}'.format(diff, unc, 1 if diff>=mini and diff<=maxi else 0)
    refposition = [int(math.ceil((len(pos)+1.0)/2)) for pos in stepsize]
    results = []
    for refstepsize in refstepsizes:
        correction = [[p-math.copysign(refstepsize, p) for p in pos] for pos in stepsize]
        cumulcorr = [[
            sum(corr[start:i])-sum(corr[i:start]) for i in range(len(corr)+1)
        ] for corr, start in zip(correction, refposition)]
        results.append([nom+corr for nom, corr in zip(nominal_bs, cumulcorr[0]+cumulcorr[1])])
    return results[0], results[1], results[2], log

def readAndCorrectDoros(filename, signs=None):
    """ input:  filename of DOROS CSV file
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: doros = [[timestamp, B1h, B1v, B2h, B2v], ...]
    """
    data, offsets = loadCsvBpm(filename)
    doros = correctDoros(data, offsets, signs)
    return doros

def readAndEvaluateDoros(filename, timestamps, horizontal, signs=None):
    """ input:  filename of DOROS CSV file
                timestamps = [[start, stop], ...]
                horizontal: True or False
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: doros_bs = [BS, BS, ...]
                doros_bs_unc = [BS_unc, BS_unc, ...]
    """
    data, offsets = loadCsvBpm(filename)
    doros, doroserr = evaluateDoros(data, offsets, timestamps, signs)
    doros_bs = [0.5*(d[0 if horizontal else 1]+d[2 if horizontal else 3]) for d in doros]
    doros_bs_unc = [0.5*(d[0 if horizontal else 1]**2+d[2 if horizontal else 3]**2)**0.5 for d in doroserr]
    return doros_bs, doros_bs_unc

def readAndApplyDoros(filename, nominal_bs, timestamps, horizontal, split, signs=None):
    """ input:  filename of DOROS CSV file
                nominal_bs = [BS, BS, ...]
                timestamps = [[start, stop], ...]
                horizontal: True or False
                split: number of steps of first scan direction
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: doros_bs = [BS, BS, ...]
                doros_bs_unc = [BS_unc, BS_unc, ...]
                nominal_corr_doros = [BS, BS, ...]
    """
    doros_bs, doros_bs_unc = readAndEvaluateDoros(filename, timestamps, horizontal, signs=signs)
    nominal_corr_doros = applyCorrectionFrom(doros_bs, nominal_bs, split)
    return doros_bs, doros_bs_unc, nominal_corr_doros

def readAndCorrectArc(filename, signs=None):
    """ input:  filename of ARC CSV file
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: arc = [[timestamp, B1h, B1v, B2h, B2v], ...]
    """
    data, offsets = loadCsvBpm(filename)
    arc = correctArc(data, offsets, signs)
    return arc

def readAndEvaluateArc(filename, nominals, timestamps, horizontal, signs=None):
    """ input:  filename of ARC CSV file
                nominals = [[B1h, B1v, B2h, B2v], ...]
                timestamps = [[start, stop], ...]
                horizontal: True or False
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: arc_bs = [BS, BS, ...]
                arc_bs_unc = [BS_unc, BS_unc, ...]
    """
    data, offsets = loadCsvBpm(filename)
    arc, arcerr = evaluateArc(data, offsets, timestamps, nominals, signs)
    arc_bs = [0.5*(d[0 if horizontal else 1]+d[2 if horizontal else 3]) for d in arc]
    arc_bs_unc = [0.5*(d[0 if horizontal else 1]**2+d[2 if horizontal else 3]**2)**0.5 for d in arcerr]
    return arc_bs, arc_bs_unc

def readAndApplyArc(filename, nominals, timestamps, horizontal, split, signs=None):
    """ input:  filename of ARC CSV file
                nominals = [[B1h, B1v, B2h, B2v], ...]
                timestamps = [[start, stop], ...]
                horizontal: True or False
                split: number of steps of first scan direction
                signs: None or [+1 +1 +1 -1 +1 +1 +1 +1]
        output: arc_bs = [BS, BS, ...]
                arc_bs_unc = [BS_unc, BS_unc, ...]
                nominal_corr_arc = [BS, BS, ...]
    """
    arc_bs, arc_bs_unc = readAndEvaluateArc(filename, nominals, timestamps, horizontal, signs=signs)
    nominal_bs = [0.5*(n[0 if horizontal else 1]+n[2 if horizontal else 3]) for n in nominals]
    nominal_corr_arc = applyCorrectionFrom(arc_bs, nominal_bs, split)
    return arc_bs, arc_bs_unc, nominal_corr_arc

def readAndSelectVertex(filename, horizontal, mode=0):
    """ input:  filename of vertex CSV file
                horizontal: True or False
                mode=0: ts_avg, =1: ts_fit, =2: ls_avg, =3: ls_fit
        output: vertex_bs = [BS, BS, ...]
                vertex_bserr = [BSerr, BSerr, ...]
    """
    vtx, vtxerr = loadCsvVtx(filename)
    vertex_bs = vtx[mode]
    if horizontal:
        vertex_bs = [-1*v for v in vertex_bs]
    vertex_bserr = vtxerr[mode]
    return vertex_bs, vertex_bserr

def readAndApplyVertex(filename, nominal_bs, horizontal, split):
    """ input:  filename of vertex CSV file
                nominal_bs = [BS, BS, ...]
                horizontal: True or False
                split: number of steps of first scan direction
        output: nominal_corr_vtx = [BS, BS, ...]
    """
    vertex_bs, vertex_bserr = readAndSelectVertex(filename, horizontal, mode=0)#-1)
    nominal_corr_vtx = applyCorrectionFrom(vertex_bs, nominal_bs, split)
    return vertex_bs, nominal_corr_vtx
