import matplotlib.pyplot as plt

import matplotlib.pyplot as plt
import numpy as np

import matplotlib.pyplot as plt
import numpy as np

# Given data
data = { #centra value, error, chi2
    #X        
#    'cls_Xfwd': (-0.0075, 0.0004, 12.2),
#    'cls_Xbkw': (-0.0053, 0.0004, 1.8),
#    'vls_X1A':  (-0.0041, 0.0003, 17.3),
#    'vls_X2A':  (-0.0033, 0.0003, 2.5),
#    'vls_X1B':  (-0.0069, 0.0003, 19.3),
#    'vls_X2B':  (-0.0030, 0.0003, 7.0)
    #Y
    'cls_Yfwd': (-0.0046, 0.0004, 34.4),
#    'cls_Ybkw': (-0.0149, 0.0004, 156.2),
    'vls_Y1A':  (-0.0042, 0.0003, 38.0),
    'vls_Y2A':  (-0.0059, 0.0003, 1.8),
    'vls_Y1B':  (-0.0023, 0.0003, 0.85),
    'vls_Y2B':  (-0.0051, 0.0003, 2.14)
}

# Extracting values, errors, and chi2
values = np.array([val[0] for val in data.values()])
errors = np.array([val[1] for val in data.values()])
chi2_values = np.array([val[2] for val in data.values()])

# Calculating avg and avgErr
avg = np.sum(values / errors ** 2) / np.sum(1 / errors ** 2)
avgErr = np.sqrt(1 / np.sum(1 / errors ** 2))
rms = np.sqrt(np.mean(values ** 2))/len(values)
half_max_dev = np.max(np.abs(values - avg))/2.
err = max(rms,half_max_dev)

# Plotting
plt.errorbar(data.keys(), values, yerr=errors, fmt='o', label='Data')
plt.axhline(y=avg, color='r', linestyle='-', label=f'avg: {avg:.4f}')
#plt.fill_between(data.keys(), avg - avgErr, avg + avgErr, color='r', alpha=0.2, label=f'avgErr: {avgErr:.4f}')
#plt.fill_between(data.keys(), avg - rms, avg + rms, color='r', alpha=0.2, label=f'rms: {rms:.4f}')
#plt.fill_between(data.keys(), avg - half_max_dev, avg + half_max_dev, color='r', alpha=0.2, label=f'half_max_dev: {half_max_dev:.4f}')
plt.fill_between(data.keys(), avg - err, avg + err, color='r', alpha=0.2, label=f'err: {err:.4f}')

# Optionally, plot chi2 values
show_chi2 = True
chi2_scale_offset = 1.1
if show_chi2:
    for i, (label, value) in enumerate(data.items()):
        plt.text(i, value[0]*chi2_scale_offset, f'chi2: {value[2]:.1f}', ha='center', va='bottom')

# Display RMS
#plt.text(0.5, avg, f'RMS: {rms:.4f}', ha='center', va='bottom')

plt.xlabel('Label')
plt.ylabel('Lenght scale')
#plt.title('Data with Average, Error Band, and Chi2')
plt.legend()
plt.xticks(rotation=45)
#plt.grid(True)
plt.show()
