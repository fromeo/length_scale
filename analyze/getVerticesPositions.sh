#!/bin/bash

# Define an array of file paths
file_paths=(
"../Fill8999/gather/fill8999_clsX.txt"
"../Fill8999/positions/fill8999_clsX.txt"

"../Fill8999/gather/fill8999_clsY.txt" 
"../Fill8999/positions/fill8999_clsY.txt"

"../Fill8999/gather/fill8999_vlsX1A.txt"  
"../Fill8999/positions/fill8999_vlsX1A.txt"

"../Fill8999/gather/fill8999_vlsX1B.txt"  
"../Fill8999/positions/fill8999_vlsX1B.txt"

"../Fill8999/gather/fill8999_vlsX2A.txt"  
"../Fill8999/positions/fill8999_vlsX2A.txt" 

"../Fill8999/gather/fill8999_vlsX2B.txt"  
"../Fill8999/positions/fill8999_vlsX2B.txt"

"../Fill8999/gather/fill8999_vlsY1A.txt"  
"../Fill8999/positions/fill8999_vlsY1A.txt"

"../Fill8999/gather/fill8999_vlsY2A.txt"
"../Fill8999/positions/fill8999_vlsY2A.txt"

"../Fill8999/gather/fill8999_vlsY1B.txt"  
"../Fill8999/positions/fill8999_vlsY1B.txt"

"../Fill8999/gather/fill8999_vlsY2B.txt"
"../Fill8999/positions/fill8999_vlsY2B.txt"
)

# Loop over all file paths in the array
for file_path in "${file_paths[@]}"; do
    # Check if the file exists
    if [ -f "$file_path" ]; then
        # Column vertex to extract (one-starting indexing)
        column_vertex=1
        column_error=2

        # Use awk to extract vertexs from the specified column and concatenate them with commas
        echo "$file_path"
        cat $file_path
        echo ""
        awk -v col=$column_vertex '{ printf("%s%s", (NR>1?", ":""), $col) } END { print "" }' "$file_path"
        echo ""
        awk -v col=$column_error '{ printf("%s%s", (NR>1?", ":""), $col) } END { print "" }' "$file_path"
        echo ""
    else
        echo "File not found: $file_path"
    fi
done
