import matplotlib.pyplot as plt

import matplotlib.pyplot as plt
import numpy as np

import matplotlib.pyplot as plt
import numpy as np

# Given data
data = { #centra value, error, chi2
    #X        
    'cls fwd': (-0.0075, 0.0004, 12.2),
    'cls bkw': (-0.0053, 0.0004, 1.8),
    'vls b2 Dn':  (-0.0041, 0.0003, 17.3),
    'vls b2 Up':  (-0.0033, 0.0003, 2.5),
    'vls b1 Dn':  (-0.0069, 0.0003, 19.3),
    'vls b1 Up':  (-0.0030, 0.0003, 7.0)
    #Y
#    'cls fwd': (-0.0046, 0.0004, 34.4),
##    'cls_Ybkw': (-0.0149, 0.0004, 156.2),
#    'vls b2 Dn':  (-0.0042, 0.0003, 38.0),
#    'vls b2 Up':  (-0.0059, 0.0003, 1.8),
#    'vls b1 Dn':  (-0.0023, 0.0003, 0.85),
#    'vls b1 Up':  (-0.0051, 0.0003, 2.14)
}
x_axis = True #False means Y axis
plt.xlabel('')
if(x_axis): plt.ylabel('Lenght scale for the X axis')
else: plt.ylabel('Lenght scale for the Y axis')

# Extracting values, errors, and chi2
values = np.array([val[0] for val in data.values()])
errors = np.array([val[1] for val in data.values()])
chi2_values = np.array([val[2] for val in data.values()])

# Calculating avg and avgErr
avg = np.sum(values / errors ** 2) / np.sum(1 / errors ** 2)
avgErr = np.sqrt(1 / np.sum(1 / errors ** 2))
rms = np.sqrt(np.mean(values ** 2))/len(values)
half_max_dev = np.max(np.abs(values - avg))/2.
err = max(rms,half_max_dev)

# Plotting
plt.errorbar(data.keys(), values, yerr=errors, fmt='o', label='Data')
plt.axhline(y=avg, color='r', linestyle='-', label=f'avg: {avg:.4f}')
#plt.fill_between(data.keys(), avg - avgErr, avg + avgErr, color='r', alpha=0.2, label=f'avgErr: {avgErr:.4f}')
#plt.fill_between(data.keys(), avg - rms, avg + rms, color='r', alpha=0.2, label=f'rms: {rms:.4f}')
#plt.fill_between(data.keys(), avg - half_max_dev, avg + half_max_dev, color='r', alpha=0.2, label=f'half_max_dev: {half_max_dev:.4f}')
plt.fill_between(data.keys(), avg - err, avg + err, color='r', alpha=0.2, label=f'RMS: {err:.4f}')

# Optionally, plot chi2 values
show_chi2 = False
chi2_scale_offset = 1.1
if show_chi2:
    for i, (label, value) in enumerate(data.items()):
        plt.text(i, value[0]*chi2_scale_offset, f'chi2: {value[2]:.1f}', ha='center', va='bottom')

title_plot = 'Fill 8999 (2023, 13.6 TeV)' #'2023 vlsX1B'
plt.title(title_plot, loc='right')

# Add text inside the plot with specific coordinates
text_x = 0.3 #np.min(values) + 0.075 * (np.max(values) - np.min(values))
text_y = np.max(values) + 0.05 * (np.max(values) - np.min(values))
#text_y = np.max(values) + 0.15 * (np.max(values) - np.min(values)) # lenght scale x axis, 'CMS preliminary' outside
#text_y = np.max(values) + 0.16 * (np.max(values) - np.min(values)) # lenght scale y axis, 'CMS preliminary' outside
plt.text(text_x, text_y, 'CMS', fontsize=14, fontweight='bold', ha='right')
if(x_axis): text_x = 1.5 #np.min(x) + 0.34 * (np.max(x) - np.min(x))
else: text_x = 1.275 #np.min(x) + 0.34 * (np.max(x) - np.min(x))
text_y = np.max(values) + 0.05 * (np.max(values) - np.min(values))
#text_y = np.max(values) + 0.15 * (np.max(values) - np.min(values)) # lenght scale x axis, 'CMS preliminary' outside
#text_y = np.max(values) + 0.16 * (np.max(values) - np.min(values)) # lenght scale y axis, 'CMS preliminary' outside
plt.text(text_x, text_y, 'Preliminary', fontsize=14, style='italic', ha='right')

# Display RMS
#plt.text(0.5, avg, f'RMS: {rms:.4f}', ha='center', va='bottom')

# Adjust margins (left, right, top, bottom)
plt.subplots_adjust(left=0.15, right=0.98, top=0.94, bottom=0.15)

#plt.title('Data with Average, Error Band, and Chi2')
if(x_axis): plt.legend(frameon=False, loc='lower center')
else: plt.legend(frameon=False, loc='upper center')
plt.xticks(rotation=45)
#plt.grid(True)

# Save the plot as a PDF file
if(x_axis): plt.savefig('lenght_scale_xaxis.pdf', format='pdf')
else: plt.savefig('lenght_scale_yaxis.pdf', format='pdf')

plt.show()
