#include "TNamed.h"

#include "prepare.cpp"

class Prepare_year2023_fill8999_resultPreliminaryAN23169_input : public Prepare {
public:
    Prepare_year2023_fill8999_resultPreliminaryAN23169_input(std::string d, std::string s) : dataset(d), source(s) {}
    const std::string dataset;
    const std::string source;

protected:
    bool condition() {
        //see https://cmsoms.cern.ch/cms/fills/report?cms_fill=8999 and https://cmsoms.cern.ch/cms/runs/report?cms_run=369802 
        //note that timestamp below has to be in UTC time as it is in CMS OMS
        //X:
        //LS 3228 starts at 2023-06-29 16:40:50 - LS 3277 ends at 2023-06-29 17:00:15
        //(1688049650.0-120<=Input.timeStamp_begin && Input.timeStamp_begin<=1688050815.0+120)
        //Y:
        //LS 3300 starts at 2023-06-29 17:08:48 - LS 3344 ends at 2023-06-29 17:26:17
        //(1688051328.0-120<=Input.timeStamp_begin && Input.timeStamp_begin<= 1688052377.0+120)
        //return ((Input.run==369802 && 3225<=Input.LS && Input.LS<=3277) || (Input.run==369802 && 3294<=Input.LS && Input.LS<=3346)); 
        return ((Input.run==369802 && 3225<=Input.LS && Input.LS<=3602)); //from lower CSL ls to higher VLS ls

        //    || (Input.timeStamp_begin>=1440476906-120 && Input.timeStamp_begin<=1440478298+120) // X2
        //    || (Input.run==369802 && Input.LS>=200 && Input.LS<=257);
        //return (Input.timeStamp_begin>=1440468555-120 && Input.timeStamp_begin<=1440470169+120) // X1
        //    || (Input.run==254991 && Input.LS>=285 && Input.LS<=360)
        //    || (Input.timeStamp_begin>=1440470289-120 && Input.timeStamp_begin<=1440471722+120) // Y1
        //    || (Input.run==254991 && Input.LS>=361 && Input.LS<=428)
        //    || (Input.timeStamp_begin>=1440476906-120 && Input.timeStamp_begin<=1440478298+120) // X2
        //    || (Input.run==254992 && Input.LS>=200 && Input.LS<=257);
    }
    void addtofile() {
        TNamed("dataset", dataset.c_str()).Write();
        TNamed("source", source.c_str()).Write();
    }
};
