#!/bin/bash

# Set the main directory path
#main_dir="/eos/cms/store/group/comm_luminosity/lcuevasp/Run3/8381/PromptReco/Nov2022_VdM-v1/"

# Define an array of subdirectories and make sure they exists
#subfolders=(
#"SpecialHLTPhysics0"  
#"SpecialHLTPhysics2"
#"SpecialHLTPhysics3"
#"SpecialHLTPhysics4"
#"SpecialHLTPhysics5" 
#"SpecialHLTPhysics6"
#"SpecialHLTPhysics7"
#"SpecialHLTPhysics8"
#"SpecialHLTPhysics9"
#"SpecialHLTPhysics11"
#"SpecialHLTPhysics12"
#"SpecialHLTPhysics13"
#"SpecialHLTPhysics14"
#"SpecialHLTPhysics15"   
## Add more subdirectories as needed
#)

main_dir="/eos/cms/store/group/comm_luminosity/lcuevasp/Run3/8999/PromptReco_Jun2023_vdM-v4/"
# Define an array of subdirectories and make sure they exists
subfolders=(
"SpecialHLTPhysics0"  
"SpecialHLTPhysics1"  
"SpecialHLTPhysics2"
"SpecialHLTPhysics3"
"SpecialHLTPhysics4"
"SpecialHLTPhysics5" 
"SpecialHLTPhysics6"
"SpecialHLTPhysics7"
"SpecialHLTPhysics8"
"SpecialHLTPhysics9"
"SpecialHLTPhysics10"
"SpecialHLTPhysics11"
"SpecialHLTPhysics12"
"SpecialHLTPhysics13"
"SpecialHLTPhysics14"
"SpecialHLTPhysics15"   
"SpecialHLTPhysics16"
"SpecialHLTPhysics17"
"SpecialHLTPhysics18"
"SpecialHLTPhysics19"
"SpecialHLTPhysics20"
"SpecialHLTPhysics21"
"SpecialHLTPhysics22"
"SpecialHLTPhysics23"
"SpecialHLTPhysics24"
"SpecialHLTPhysics25"   
"SpecialHLTPhysics26"
"SpecialHLTPhysics27"
"SpecialHLTPhysics28"
"SpecialHLTPhysics29"
"SpecialHLTPhysics30"
"SpecialHLTPhysics31"
# Add more subdirectories as needed
)

# Loop through each subfolder
for subfolder in "${subfolders[@]}"; do
    # Initialize the path with the main directory
    path="$main_dir$subfolder/"

    # Find the chain of subdirectories until files appear
    while true; do
        # Check if the path contains files
        if [ -n "$(find "$path" -maxdepth 1 -type f)" ]; then
            #echo "Files found in: $path"
            result_to_print=$(echo "$path" | sed "s|$main_dir||")
            echo "\"$result_to_print\""
            break
        fi

        # Get the next level of subdirectories
        next_dir="$(find "$path" -mindepth 1 -maxdepth 1 -type d -printf '%f\n')"

        # Check if there's only one subdirectory
        if [ "$(echo "$next_dir" | wc -l)" -eq 1 ]; then
            path="$path$next_dir/"
        else
            echo "Error: Multiple subdirectories found in $path"
            break
        fi
    done
done
