#!/bin/bash

# Set the path to the main directory
#main_dir="/eos/cms/store/group/comm_luminosity/lcuevasp/Run3/8381/PromptReco/Nov2022_VdM-v1/"

## Define an array of subdirectories
#dirs=(
#"SpecialHLTPhysics0/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_224954/0000/"
#"SpecialHLTPhysics2/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225033/0000/"
#"SpecialHLTPhysics3/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225157/0000/"
#"SpecialHLTPhysics4/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225230/0000/"
#"SpecialHLTPhysics5/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225312/0000/"
#"SpecialHLTPhysics6/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225353/0000/"
#"SpecialHLTPhysics7/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225435/0000/"
#"SpecialHLTPhysics8/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225511/0000/"
#"SpecialHLTPhysics9/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_225901/0000/"
#"SpecialHLTPhysics11/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_230021/0000/"
#"SpecialHLTPhysics12/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_230118/0000/"
#"SpecialHLTPhysics13/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_230153/0000/"
#"SpecialHLTPhysics14/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_230234/0000/"
#"SpecialHLTPhysics15/crab_CMSSW_12_4_9_LumiPixelsMinBias_splitPerBXTrue/221129_230310/0000/"
#)

main_dir="/eos/cms/store/group/comm_luminosity/lcuevasp/Run3/8999/PromptReco_Jun2023_vdM-v4/"
dirs=(
"SpecialHLTPhysics0/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_0/230905_230941/0000/"
"SpecialHLTPhysics1/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_1/230905_231221/0000/"
"SpecialHLTPhysics2/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_2/230905_232015/0000/"
"SpecialHLTPhysics3/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_3/230905_232056/0000/"
"SpecialHLTPhysics4/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_4/230905_232158/0000/"
"SpecialHLTPhysics5/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_5/230905_232314/0000/"
"SpecialHLTPhysics6/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_6/230905_232419/0000/"
"SpecialHLTPhysics7/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_7/230905_232500/0000/"
"SpecialHLTPhysics8/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_8/230905_232729/0000/"
"SpecialHLTPhysics9/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_9/230905_232839/0000/"
"SpecialHLTPhysics10/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_10/230905_232933/0000/"
"SpecialHLTPhysics11/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_11/230905_233117/0000/"
"SpecialHLTPhysics12/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_12/230905_233208/0000/"
"SpecialHLTPhysics13/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_13/230905_233249/0000/"
"SpecialHLTPhysics14/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_14/230905_233334/0000/"
"SpecialHLTPhysics15/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_15/230905_233428/0000/"
"SpecialHLTPhysics16/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_16/230905_233540/0000/"
"SpecialHLTPhysics17/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_17/230905_233620/0000/"
"SpecialHLTPhysics18/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_18/230905_233734/0000/"
"SpecialHLTPhysics19/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_19/230905_233816/0000/"
"SpecialHLTPhysics20/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_20/230905_233905/0000/"
"SpecialHLTPhysics21/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_21/230905_233948/0000/"
"SpecialHLTPhysics22/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_22/230905_234143/0000/"
"SpecialHLTPhysics23/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_23/230905_234230/0000/"
"SpecialHLTPhysics24/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_24/230905_234312/0000/"
"SpecialHLTPhysics25/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_25/230905_234427/0000/"
"SpecialHLTPhysics26/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_26/230905_234544/0000/"
"SpecialHLTPhysics27/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_27/230905_234722/0000/"
"SpecialHLTPhysics28/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_28/230905_234814/0000/"
"SpecialHLTPhysics29/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_29/230905_234854/0000/"
"SpecialHLTPhysics30/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_30/230905_234940/0000/"
"SpecialHLTPhysics31/crab_crab_8999_CMSSW_13_0_7_TOTEM_LumiPixelsMinBias_31/230905_235029/0000/"
)

# Loop through each directory
for ((i=0; i<${#dirs[@]}; i++)); do
    # Generate the output with "ZeroBias" followed by the index
    echo "//ZeroBias$i"
    echo -n "{"

    # List root files and append them to the output
    files=($(ls -v "$main_dir${dirs[i]}"/*.root)) 
    for file in "${files[@]}"; do
        echo -n "\"$(basename "$file")\","
    done

    # Remove the trailing comma, if any
    echo "},"
done
